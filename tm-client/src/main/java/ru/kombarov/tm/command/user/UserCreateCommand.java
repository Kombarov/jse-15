package ru.kombarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.Role;
import ru.kombarov.tm.api.endpoint.UserDTO;
import ru.kombarov.tm.command.AbstractCommand;

public final class UserCreateCommand extends AbstractCommand {

    @Override
    public @NotNull String command() {
        return "user-create";
    }

    @Override
    public @NotNull String description() {
        return "Create new User.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CREATE]");
        System.out.println("ENTER USERNAME");
        if (serviceLocator == null) throw new Exception();
        @Nullable String name = input.readLine();
        for (final @NotNull UserDTO user : serviceLocator.getUserEndpoint().findAllUsers(serviceLocator.getSessionDTO())) {
            if (user.getLogin() == null || user.getLogin().isEmpty()) return;
            while (user.getLogin().equals(name)) {
                System.out.println("THIS LOGIN EXISTS");
                System.out.println("ENTER ANOTHER NAME");
                name = input.readLine();
            }
        }
        System.out.println("TYPE YOUR ROLE: USER OR ADMINISTRATOR");
        final @NotNull Role role = Role.valueOf((input.readLine()).toUpperCase());
        System.out.println("ENTER PASSWORD");
        final @Nullable String password = input.readLine();
        serviceLocator.getUserEndpoint().persistUser(name, password, role);
        System.out.println("[OK]");
    }
}
