package ru.kombarov.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

public class DataJaxBJSONLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data JaxB json load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data by JaxB from JSON format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JAXB JSON LOAD]");
        if (serviceLocator == null) return;
        System.out.println("[OK]");
    }
}
