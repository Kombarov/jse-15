package ru.kombarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.api.endpoint.ProjectDTO;
import ru.kombarov.tm.api.endpoint.Status;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.DateUtil.parseStringToDate;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER PROJECT NAME");
        final @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(input.readLine());
        project.setStatus(Status.PLANNED);
        System.out.println("ENTER PROJECT DESCRIPTION");
        project.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        project.setDateStart(parseStringToDate(input.readLine()));
        System.out.println("ENTER FINISH DATE");
        project.setDateFinish(parseStringToDate(input.readLine()));
        if (serviceLocator == null) throw new Exception();
        project.setUserId(serviceLocator.getSessionDTO().getUserId());
        serviceLocator.getProjectEndpoint().persistProject(serviceLocator.getSessionDTO(), project);
        System.out.println("[OK]");
    }
}
