package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CLEAR]");
        if (serviceLocator == null) throw new Exception();
        serviceLocator.getTaskEndpoint().removeAllTasksByUserId(serviceLocator.getSessionDTO());
        System.out.println("[OK]");
    }
}
