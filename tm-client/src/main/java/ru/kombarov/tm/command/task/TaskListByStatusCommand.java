package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.api.endpoint.Status;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.util.EntityUtil;

public class TaskListByStatusCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list show by status";
    }

    @NotNull
    @Override
    public String description() {
        return "Show tasks by status.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST BY STATUS]");
        System.out.println(Status.PLANNED.value() + ":");
        if (serviceLocator == null) throw new Exception();
        EntityUtil.printTasks(serviceLocator.getTaskEndpoint().sortTasksByStatus(serviceLocator.getSessionDTO(), serviceLocator.getSessionDTO().getUserId()));
        System.out.println("[OK]");
    }
}
