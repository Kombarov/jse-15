package ru.kombarov.tm.command.data.save;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

public class DataFasterXmlJSONSaveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data fasterXml json save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data by FasterXML to JSON format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA FASTERXML JSON SAVE]");
        if (serviceLocator == null) return;
        System.out.println("[OK]");
    }
}
