package ru.kombarov.tm.command.data.load;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

public class DataFasterXmlJSONLoadCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "data fasterXml json load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data by FasterXML from JSON format.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA FASTERXML JSON LOAD]");
        if (serviceLocator == null) return;
        System.out.println("[OK]");
    }
}
