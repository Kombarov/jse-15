package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.api.endpoint.Status;
import ru.kombarov.tm.api.endpoint.TaskDTO;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.DateUtil.parseStringToDate;

public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER TASK NAME");
        final @NotNull TaskDTO task = new TaskDTO();
        task.setName(input.readLine());
        task.setStatus(Status.PLANNED);
        System.out.println("ENTER TASK DESCRIPTION");
        task.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        task.setDateStart(parseStringToDate(input.readLine()));
        System.out.println("ENTER FINISH DATE");
        task.setDateFinish(parseStringToDate(input.readLine()));
        if (serviceLocator == null) throw new Exception();
        task.setUserId(serviceLocator.getSessionDTO().getUserId());
        serviceLocator.getTaskEndpoint().persistTask(serviceLocator.getSessionDTO(), task);
        System.out.println("[OK]");
    }
}
