package ru.kombarov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.*;

public interface ServiceLocator {

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    @NotNull
    ISessionEndpoint getSessionEndpoint();

    @Nullable
    SessionDTO getSessionDTO();

    void setSessionDTO(final @Nullable SessionDTO sessionDTO);
}
