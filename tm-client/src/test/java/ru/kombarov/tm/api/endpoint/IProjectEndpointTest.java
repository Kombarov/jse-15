package ru.kombarov.tm.api.endpoint;

import junit.framework.TestCase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.experimental.categories.Category;
import ru.kombarov.tm.endpoint.ProjectEndpointService;
import ru.kombarov.tm.endpoint.SessionEndpointService;
import ru.kombarov.tm.endpoint.UserEndpointService;

@Category(IntegrationTest.class)
public class IProjectEndpointTest extends TestCase {

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Nullable
    private SessionDTO session;

    @NotNull
    private final UserDTO test = new UserDTO();

    @NotNull
    private final ProjectDTO project1 = new ProjectDTO();

    @NotNull
    private final ProjectDTO project2 = new ProjectDTO();

    @NotNull
    private final ProjectDTO project3 = new ProjectDTO();

    public void testFindAllProjectsByUserId() throws Exception_Exception {
        initProjects();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        projectEndpoint.persistProject(session, project1);
        projectEndpoint.persistProject(session, project2);
        assertEquals(project1.getDescription(), projectEndpoint.findAllProjectsByUserId(session).get(0).getDescription());
        assertEquals(project2.getDescription(), projectEndpoint.findAllProjectsByUserId(session).get(1).getDescription());
        projectEndpoint.removeAllProjects(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testRemoveAllProjectsByUserId() throws Exception_Exception {
        initProjects();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        projectEndpoint.persistProject(session, project1);
        projectEndpoint.persistProject(session, project2);
        projectEndpoint.findAllProjectsByUserId(session);
        assertEquals(0, projectEndpoint.findAllProjectsByUserId(session).size());
        projectEndpoint.removeAllProjects(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testFindProjectByName() {
    }

    public void testPersistProject() throws Exception_Exception {
        initProjects();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        projectEndpoint.persistProject(session, project1);
        assertEquals(1, projectEndpoint.findAllProjects(session).size());
        projectEndpoint.removeAllProjects(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testRemoveProject() throws Exception_Exception {
        initProjects();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        projectEndpoint.persistProject(session, project1);
        projectEndpoint.persistProject(session, project2);
        projectEndpoint.removeProject(session, project1.getId());
        assertEquals(1, projectEndpoint.findAllProjects(session).size());
        projectEndpoint.removeAllProjects(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testFindOneProjectByUserId() throws Exception_Exception {
        initProjects();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        projectEndpoint.persistProject(session, project1);
        projectEndpoint.persistProject(session, project2);
        assertEquals(test.getId(), projectEndpoint.findOneProjectByUserId(session, test.getId()).getUserId());
        projectEndpoint.removeAllProjects(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testMergeProject() throws Exception_Exception {
        initProjects();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        projectEndpoint.persistProject(session, project1);
        projectEndpoint.mergeProject(session, project1);
        assertEquals(1, projectEndpoint.findAllProjects(session).size());
        projectEndpoint.removeAllProjects(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testFindProjectsByPart() throws Exception_Exception {
        initProjects();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        projectEndpoint.persistProject(session, project1);
        assertEquals(1, projectEndpoint.findProjectsByPart(session, "descr").size());
        projectEndpoint.removeAllProjects(session);
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void initProjects() {
        test.setLogin("admin");
        test.setPassword("gffdhde");
        test.setRole(Role.ADMINISTRATOR);

        project1.setUserId(test.getId());
        project1.setName("Name1");
        project1.setDescription("description1");
        project1.setStatus(Status.PLANNED);

        project2.setUserId(test.getId());
        project2.setName("Name2");
        project2.setDescription("description2");
        project2.setStatus(Status.DONE);

        project3.setUserId(test.getId());
        project3.setName("Name3");
        project3.setDescription("description3");
        project3.setStatus(Status.IN_PROCESS);
    }
}