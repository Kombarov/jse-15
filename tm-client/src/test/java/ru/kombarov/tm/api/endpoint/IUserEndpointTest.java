package ru.kombarov.tm.api.endpoint;

import junit.framework.TestCase;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.experimental.categories.Category;
import ru.kombarov.tm.endpoint.SessionEndpointService;
import ru.kombarov.tm.endpoint.UserEndpointService;

@Category(IntegrationTest.class)
public class IUserEndpointTest extends TestCase {

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @Nullable
    private SessionDTO session;

    @NotNull
    private final UserDTO test = new UserDTO();

    @NotNull
    private final UserDTO user1 = new UserDTO();

    @NotNull
    private final UserDTO user2 = new UserDTO();

    @NotNull
    private final UserDTO user3 = new UserDTO();

    public void testMergeUser() throws Exception_Exception {
        initUsers();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        userEndpoint.persistUser(user1.getLogin(), user1.getPassword(), user1.getRole());
        userEndpoint.persistUser(user1.getLogin(), user1.getPassword(), user1.getRole());
        assertEquals(2, userEndpoint.findAllUsers(session).size());
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testFindOneUser() throws Exception_Exception {
        initUsers();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        userEndpoint.persistUser(user1.getLogin(), user1.getPassword(), user1.getRole());
        userEndpoint.persistUser(user2.getLogin(), user2.getPassword(), user2.getRole());
        userEndpoint.persistUser(user3.getLogin(), user3.getPassword(), user3.getRole());
        assertEquals(user1.login, userEndpoint.findOneUser(session, user1.getId()).getLogin());
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testPersistUser() throws Exception_Exception {
        initUsers();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        userEndpoint.persistUser(user1.getLogin(), user1.getPassword(), user1.getRole());
        userEndpoint.persistUser(user2.getLogin(), user2.getPassword(), user2.getRole());
        userEndpoint.persistUser(user3.getLogin(), user3.getPassword(), user3.getRole());
        assertEquals(4, userEndpoint.findAllUsers(session).size());
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testGetUserByUserName() throws Exception_Exception {
        initUsers();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        userEndpoint.persistUser(user1.getLogin(), user1.getPassword(), user1.getRole());
        userEndpoint.persistUser(user2.getLogin(), user2.getPassword(), user2.getRole());
        userEndpoint.persistUser(user3.getLogin(), user3.getPassword(), user3.getRole());
        assertEquals(user1.getLogin(), userEndpoint.getUserByUserName(session, user1.getLogin()).getLogin());
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void testRemoveUser() throws Exception_Exception {
        initUsers();
        userEndpoint.persistUser(test.getLogin(), test.getPassword(), test.getRole());
        session = sessionEndpoint.createSession(test.getLogin(), test.getPassword());
        userEndpoint.persistUser(user1.getLogin(), user1.getPassword(), user1.getRole());
        userEndpoint.persistUser(user2.getLogin(), user2.getPassword(), user2.getRole());
        userEndpoint.persistUser(user3.getLogin(), user3.getPassword(), user3.getRole());
        userEndpoint.removeUser(session, user1.getId());
        assertEquals(3, userEndpoint.findAllUsers(session).size());
        userEndpoint.removeAllUsers(session);
        sessionEndpoint.removeSession(session.getUserId(), session.getId());
    }

    public void initUsers() {
        test.setLogin("admin");
        test.setPassword("gffdhde");
        test.setRole(Role.ADMINISTRATOR);

        user1.setLogin("Name1");
        user1.setPassword("gfde");
        user1.setRole(Role.USER);

        user2.setLogin("Name2");
        user2.setPassword("gfsdfde");
        user2.setRole(Role.USER);

        user3.setLogin("Name3");
        user3.setPassword("45896");
        user3.setRole(Role.USER);
    }
}