package ru.kombarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;

public abstract class AbstractRepository {

    @NotNull
    protected EntityManager em;

    public AbstractRepository() {
    }

    @NotNull
    public AbstractRepository(final @NotNull EntityManager entityManager) {
        this.em = entityManager;
    }
}
