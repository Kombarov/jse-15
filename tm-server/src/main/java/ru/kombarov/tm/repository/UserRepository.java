package ru.kombarov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.user.IUserRepository;
import ru.kombarov.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository implements IUserRepository {

    public UserRepository(final @NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void persist(final @NotNull User user) {
        em.persist(user);
    }

    @Override
    public void merge(final @NotNull User user) {
        em.merge(user);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return em.createQuery("SELECT u FROM User u", User.class).getResultList();
    }

    @Nullable
    @Override
    public User findOne(final @NotNull String id) {
        return em.find(User.class, id);
    }

    @Override
    public void remove(final @NotNull String id) {
        em.remove(findOne(id));
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM User ").executeUpdate();
    }

    @Nullable
    @Override
    public User getUserByLogin(final @NotNull String login) {
        return em.createQuery("SELECT u FROM User u WHERE u.login = :login",
                               User.class).setParameter("login", login).getSingleResult();
    }
}
