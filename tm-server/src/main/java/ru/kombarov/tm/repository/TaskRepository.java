package ru.kombarov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.task.ITaskRepository;
import ru.kombarov.tm.entity.Task;

import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository implements ITaskRepository {

    public TaskRepository(final @NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void persist(final @NotNull Task task) {
        em.persist(task);
    }

    @Override
    public void merge(final @NotNull Task task) {
        em.merge(task);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return em.createQuery("SELECT t FROM Task t", Task.class).getResultList();
    }

    @Nullable
    @Override
    public Task findOne(final @NotNull String id) {
        return em.find(Task.class, id);
    }

    @Override
    public void remove(final @NotNull String id) {
        em.remove(findOne(id));
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM Task").executeUpdate();
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(final @NotNull String userId) {
        return em.createQuery("SELECT t FROM Task t WHERE t.userId= :userId",
                               Task.class).setParameter("userId", userId).getResultList();
    }

    @Nullable
    @Override
    public Task findOneByUserId(final @NotNull String userId, final @NotNull String id) {
        return em.createQuery("SELECT t FROM Task t WHERE t.id = :id and t.userId = :userId",
                               Task.class).setParameter("id", id).setParameter("userId", userId).getSingleResult();
    }

    @Override
    public void removeAllByUserId(final @NotNull String userId) {
        em.createQuery("DELETE FROM Task t WHERE t.userId = :userId").setParameter("userId", userId).executeUpdate();
    }

    @NotNull
    @Override
    public Task findByName(final @NotNull String userId, final @NotNull String name) {
        return em.createQuery("SELECT t FROM Task t WHERE t.userId= :userId AND t.name = :name",
                               Task.class).setParameter("userId", userId).setParameter("name", name).getSingleResult();
    }

    @NotNull
    @Override
    public List<Task> getTasksByProjectId(final @NotNull String userId, final @NotNull String projectId) {
        return em.createQuery("SELECT t FROM Task t WHERE t.userId = :userId and t.projectId = :projectId",
                               Task.class).setParameter("userId", userId).setParameter("projectId", projectId).getResultList();
    }

    @NotNull
    @Override
    public List<Task> sortByDateStart(final @NotNull String userId) {
        return em.createQuery("SELECT t FROM Task t WHERE t.userId= :userId ORDER BY t.dateStart",
                               Task.class).setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<Task> sortByDateFinish(final @NotNull String userId) {
        return em.createQuery("SELECT t FROM Task t WHERE t.userId= :userId ORDER BY t.dateFinish",
                               Task.class).setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<Task> sortByStatus(final @NotNull String userId) {
        return em.createQuery("SELECT t FROM Task t WHERE t.userId= :userId ORDER BY FIELD(t.status, 'PLANNED', 'IN_PROCESS', 'DONE')",
                               Task.class).setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findByPart(final @NotNull String description, final @NotNull String userId) {
        return em.createQuery("SELECT t FROM Task t WHERE t.userId= :userId AND t.description = :description",
                               Task.class).setParameter("userId", userId).setParameter("description", description).getResultList();
    }
}
