package ru.kombarov.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.ServiceLocator;
import ru.kombarov.tm.endpoint.ProjectEndpoint;
import ru.kombarov.tm.endpoint.SessionEndpoint;
import ru.kombarov.tm.endpoint.TaskEndpoint;
import ru.kombarov.tm.endpoint.UserEndpoint;
import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.service.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.xml.ws.Endpoint;
import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @Getter
    @NotNull
    private final ProjectService projectService = new ProjectService(this);

    @Getter
    @NotNull
    private final TaskService taskService = new TaskService(this);

    @Getter
    @NotNull
    private final UserService userService = new UserService(this);

    @Getter
    @NotNull
    private final SessionService sessionService = new SessionService(this);

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    @Nullable
    private EntityManagerFactory entityManagerFactory;

    private void setEntityManagerFactory(final @NotNull EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @NotNull
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    private EntityManagerFactory getEntityManagerFactory() throws Exception {
        final @NotNull Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, propertyService.getDriver());
        settings.put(org.hibernate.cfg.Environment.URL, propertyService.getUrl());
        settings.put(org.hibernate.cfg.Environment.USER, propertyService.getLogin());
        settings.put(org.hibernate.cfg.Environment.PASS, propertyService.getPassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, "update");
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, "true");
        final @NotNull StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final @NotNull StandardServiceRegistry registry = registryBuilder.build();
        final @NotNull MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        final @NotNull Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    public void init() throws Exception {
        Endpoint.publish("http://localhost:8080/projectService?wsdl", projectEndpoint);
        Endpoint.publish("http://localhost:8080/taskService?wsdl", taskEndpoint);
        Endpoint.publish("http://localhost:8080/userService?wsdl", userEndpoint);
        Endpoint.publish("http://localhost:8080/sessionService?wsdl", sessionEndpoint);
        setEntityManagerFactory(getEntityManagerFactory());
    }
}