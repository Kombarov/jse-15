package ru.kombarov.tm.datatransfer;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.enumerated.Role;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public final class SessionDTO extends AbstractEntityDTO {

    @NotNull
    String userId;

    @NotNull
    Role role;

    @NotNull
    Long timestamp = new Date().getTime();

    @Nullable
    String signature;

    @NotNull
    public static Session toSession(final @NotNull Bootstrap bootstrap, final @NotNull SessionDTO sessionDTO) {
        final @NotNull Session session = new Session();
        session.setId(sessionDTO.getId());
        session.setUserId(sessionDTO.getUserId());
        session.setRole(sessionDTO.getRole());
        session.setTimestamp(sessionDTO.getTimestamp());
        session.setSignature(sessionDTO.getSignature());
        return session;
    }
}
