package ru.kombarov.tm.datatransfer;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.enumerated.Role;

@Setter
@Getter
@NoArgsConstructor
public final class UserDTO extends AbstractEntityDTO {

    @NotNull
    private String login;

    @NotNull
    private String password;

    @NotNull
    private Role role;

    @NotNull
    public static User toUser(final @NotNull Bootstrap bootstrap, final @NotNull UserDTO userDTO) {
        final @NotNull User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPassword(userDTO.getPassword());
        user.setRole(userDTO.getRole());
        return user;
    }
}
