package ru.kombarov.tm.datatransfer;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.enumerated.Status;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public final class TaskDTO extends AbstractEntityDTO {

    @Nullable
    private String name;

    @Nullable
    private String projectId;

    @NotNull
    private String userId;

    @Nullable
    private String description;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    public static Task toTask(final @NotNull Bootstrap bootstrap, final @NotNull TaskDTO taskDTO) {
        final @NotNull Task task = new Task();
        task.setId(taskDTO.getId());
        task.setName(taskDTO.getName());
        task.setProjectId(taskDTO.getProjectId());
        task.setUserId(taskDTO.getUserId());
        task.setDescription(taskDTO.getDescription());
        task.setDateStart(taskDTO.getDateStart());
        task.setDateFinish(taskDTO.getDateFinish());
        task.setStatus(taskDTO.getStatus());
        return task;
    }
}
