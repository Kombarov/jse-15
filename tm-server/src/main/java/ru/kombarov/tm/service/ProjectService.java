package ru.kombarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.project.IProjectService;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import java.util.List;

public final class ProjectService implements IProjectService {

    @NotNull
    private final Bootstrap bootstrap;

    public ProjectService(final @NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void persist(final @Nullable Project project) throws Exception {
        if (project == null) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        final @NotNull EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        final @NotNull ProjectRepository projectRepository = new ProjectRepository(em);
        projectRepository.persist(project);
        transaction.commit();
        em.close();
    }

    @Override
    public void merge(final @Nullable Project project) throws Exception {
        if (project == null) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        final @NotNull EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        final @NotNull ProjectRepository projectRepository = new ProjectRepository(em);
        projectRepository.merge(project);
        transaction.commit();
        em.close();
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull List<Project> projects = new ProjectRepository(em).findAll();
        em.getTransaction().commit();
        em.close();
        return projects;
    }

    @Nullable
    @Override
    public Project findOne(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @Nullable Project project = new ProjectRepository(em).findOne(id);
        em.getTransaction().commit();
        em.close();
        return project;
    }

    @Override
    public void remove(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        final @NotNull EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        final @Nullable Project project = findOne(id);
        if(project == null) return;
        final @NotNull ProjectRepository projectRepository = new ProjectRepository(em);
        projectRepository.remove(id);
        transaction.commit();
        em.close();
    }

    @Override
    public void removeAll() {
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull ProjectRepository projectRepository = new ProjectRepository(em);
        projectRepository.removeAll();
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public List<Project> findAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull List<Project> projects = new ProjectRepository(em).findAllByUserId(userId);
        em.getTransaction().commit();
        em.close();
        return projects;
    }

    @Nullable
    @Override
    public Project findOne(final @Nullable String userId,final  @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (id == null || id.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @Nullable Project project;
        try {
            project = new ProjectRepository(em).findOneByUserId(userId, id);
        }
        catch (NoResultException e) {
            return null;
        }
        em.getTransaction().commit();
        em.close();
        return project;
    }

    @Override
    public void removeAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull ProjectRepository projectRepository = new ProjectRepository(em);
        projectRepository.removeAllByUserId(userId);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public Project findByName(final @Nullable String userId, final @Nullable String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (name == null || name.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull Project project = new ProjectRepository(em).findByName(userId, name);
        em.getTransaction().commit();
        em.close();
        return project;
    }

    @NotNull
    @Override
    public List<Project> sortByDateStart(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull List<Project> projects = new ProjectRepository(em).sortByDateStart(userId);
        em.getTransaction().commit();
        em.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> sortByDateFinish(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull List<Project> projects = new ProjectRepository(em).sortByDateFinish(userId);
        em.getTransaction().commit();
        em.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> sortByStatus(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull List<Project> projects = new ProjectRepository(em).sortByStatus(userId);
        em.getTransaction().commit();
        em.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findByPart(final @Nullable String description, final @Nullable String userId) throws Exception {
        if (description == null || description.isEmpty()) throw new Exception();
        if (userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull List<Project> projects = new ProjectRepository(em).findByPart(description, userId);
        em.getTransaction().commit();
        em.close();
        return projects;
    }
}