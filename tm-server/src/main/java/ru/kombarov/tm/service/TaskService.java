package ru.kombarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.task.ITaskService;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.entity.Task;
import ru.kombarov.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import java.util.List;

public final class TaskService implements ITaskService {

    @NotNull
    private final Bootstrap bootstrap;

    public TaskService(final @NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void persist(final @Nullable Task task) throws Exception {
        if (task == null) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        final @NotNull EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        final @NotNull TaskRepository taskRepository = new TaskRepository(em);
        taskRepository.persist(task);
        transaction.commit();
        em.close();
    }

    @Override
    public void merge(final @Nullable Task task) throws Exception {
        if (task == null) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        final @NotNull EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        final @NotNull TaskRepository taskRepository = new TaskRepository(em);
        taskRepository.merge(task);
        transaction.commit();
        em.close();
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull List<Task> tasks = new TaskRepository(em).findAll();
        em.getTransaction().commit();
        em.close();
        return tasks;
    }

    @Nullable
    @Override
    public Task findOne(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @Nullable Task task = new TaskRepository(em).findOne(id);
        em.getTransaction().commit();
        em.close();
        return task;
    }

    @Override
    public void remove(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        final @NotNull EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        final @NotNull TaskRepository taskRepository = new TaskRepository(em);
        taskRepository.remove(id);
        transaction.commit();
        em.close();
    }

    @Override
    public void removeAll() {
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull TaskRepository taskRepository = new TaskRepository(em);
        taskRepository.removeAll();
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public List<Task> findAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull List<Task> tasks = new TaskRepository(em).findAllByUserId(userId);
        em.getTransaction().commit();
        em.close();
        return tasks;
    }

    @Nullable
    @Override
    public Task findOne(final @Nullable String userId, final @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (id == null || id.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @Nullable Task task;
        try {
            task = new TaskRepository(em).findOneByUserId(userId, id);
        }
        catch (NoResultException e) {
            return null;
        }
        em.getTransaction().commit();
        em.close();
        return task;
    }

    @Override
    public void removeAll(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull TaskRepository taskRepository = new TaskRepository(em);
        taskRepository.removeAllByUserId(userId);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public Task findByName(final @Nullable String userId, final @Nullable String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (name == null || name.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull Task task = new TaskRepository(em).findByName(userId, name);
        em.getTransaction().commit();
        em.close();
        return task;
    }

    @NotNull
    @Override
    public List<Task> getTasksByProjectId(final @Nullable String userId, final @Nullable String projectId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull List<Task> tasks = new TaskRepository(em).getTasksByProjectId(userId, projectId);
        em.getTransaction().commit();
        em.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> sortByDateStart(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull List<Task> tasks = new TaskRepository(em).sortByDateStart(userId);
        em.getTransaction().commit();
        em.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> sortByDateFinish(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull List<Task> tasks = new TaskRepository(em).sortByDateFinish(userId);
        em.getTransaction().commit();
        em.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> sortByStatus(final @Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull List<Task> tasks = new TaskRepository(em).sortByStatus(userId);
        em.getTransaction().commit();
        em.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findByPart(final @Nullable String description, final @Nullable String userId) throws Exception {
        if (description == null || description.isEmpty()) throw new Exception();
        if (userId == null || userId.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull List<Task> tasks = new TaskRepository(em).findByPart(description, userId);
        em.getTransaction().commit();
        em.close();
        return tasks;
    }
}