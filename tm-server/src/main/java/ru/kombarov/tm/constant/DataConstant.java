package ru.kombarov.tm.constant;

import org.jetbrains.annotations.NotNull;

public class DataConstant {

    public static final @NotNull String FILE_BINARY = "./data.bin";

    public static final @NotNull String FILE_XML = "./data.xml";

    public static final @NotNull String FILE_JSON = "./data.json";

    public static final @NotNull String FILE_PROPERTIES = "./tm-server/src/main/resources/config.properties";
}
