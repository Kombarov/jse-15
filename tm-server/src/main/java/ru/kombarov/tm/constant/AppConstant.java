package ru.kombarov.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class AppConstant {

    public final static @NotNull String SALT = "jlsglpowf";

    public final static @NotNull Integer CICLE = 25;

    public final static @NotNull Integer SESSION_LIFE_TIME = 800000;
}
