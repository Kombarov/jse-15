package ru.kombarov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.ISessionEndpoint;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.datatransfer.SessionDTO;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;

import static ru.kombarov.tm.constant.AppConstant.CICLE;
import static ru.kombarov.tm.constant.AppConstant.SALT;

@NoArgsConstructor
@WebService(endpointInterface = "ru.kombarov.tm.api.endpoint.ISessionEndpoint")
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    public SessionEndpoint(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    @Nullable
    @WebMethod
    public SessionDTO createSession(final @NotNull String login, final @NotNull String password) throws Exception {
        @Nullable final User user = bootstrap.getUserService().getUserByLogin(login);
        if(user == null) return null;
        if(!user.getPassword().equals(SignatureUtil.sign(password, SALT, CICLE))) return null;
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(user.getId());
        sessionDTO.setRole(user.getRole());
        final @NotNull Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        session.setSignature(SignatureUtil.sign(session, SALT, CICLE));
        bootstrap.getSessionService().persist(session);
        return sessionDTO;
    }

    @Override
    @WebMethod
    public void removeSession(final @Nullable String userId, final @Nullable String id) throws Exception {
        bootstrap.getSessionService().remove(userId, id);
    }
}
