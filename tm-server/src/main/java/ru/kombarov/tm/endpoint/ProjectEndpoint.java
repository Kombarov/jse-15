package ru.kombarov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.IProjectEndpoint;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.datatransfer.ProjectDTO;
import ru.kombarov.tm.datatransfer.SessionDTO;
import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.kombarov.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(final @NotNull Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    @WebMethod
    public void persistProject(final @Nullable SessionDTO sessionDTO, final @Nullable ProjectDTO projectDTO) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getProjectService().persist(ProjectDTO.toProject(bootstrap, projectDTO));
    }

    @Override
    @WebMethod
    public void mergeProject(final @Nullable SessionDTO sessionDTO, final @Nullable ProjectDTO projectDTO) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getProjectService().merge(ProjectDTO.toProject(bootstrap, projectDTO));
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjects(final @Nullable SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        return Project.toProjectsDTO(bootstrap.getProjectService().findAll());
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDTO findOneProject(final @Nullable SessionDTO sessionDTO, final @Nullable String id) throws Exception {
        validateSession(sessionDTO);
        return Project.toProjectDTO(bootstrap.getProjectService().findOne(id));
    }

    @Override
    @WebMethod
    public void removeProject(final @Nullable SessionDTO sessionDTO, final @Nullable String id) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getProjectService().remove(id);
    }

    @Override
    @WebMethod
    public void removeAllProjects(final @Nullable SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getProjectService().removeAll();
    }

    @NotNull
    @WebMethod
    public ProjectDTO findProjectByName(final @Nullable SessionDTO sessionDTO, final @Nullable String name) throws Exception {
        final @NotNull Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Project.toProjectDTO(bootstrap.getProjectService().findByName(session.getUserId(), name));
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjectsByUserId(final @Nullable SessionDTO sessionDTO) throws Exception {
        final @NotNull Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Project.toProjectsDTO(bootstrap.getProjectService().findAll(session.getUserId()));
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDTO findOneProjectByUserId(final @Nullable SessionDTO sessionDTO, final @Nullable String id) throws Exception {
        final @NotNull Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Project.toProjectDTO(bootstrap.getProjectService().findOne(session.getUserId(), id));
    }

    @Override
    @WebMethod
    public void removeAllProjectsByUserId(final @Nullable SessionDTO sessionDTO) throws Exception {
        final @NotNull Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        bootstrap.getProjectService().removeAll(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> sortProjectsByDateStart(final @Nullable SessionDTO sessionDTO, final @Nullable String userId) throws Exception {
        validateSession(sessionDTO);
        return Project.toProjectsDTO(bootstrap.getProjectService().sortByDateStart(userId));
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> sortProjectsByDateFinish(final @Nullable SessionDTO sessionDTO, final @Nullable String userId) throws Exception {
        validateSession(sessionDTO);
        return Project.toProjectsDTO(bootstrap.getProjectService().sortByDateFinish(userId));
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> sortProjectsByStatus(final @Nullable SessionDTO sessionDTO, final @Nullable String userId) throws Exception {
        validateSession(sessionDTO);
        return Project.toProjectsDTO(bootstrap.getProjectService().sortByStatus(userId));
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> findProjectsByPart(final @Nullable SessionDTO sessionDTO, final @Nullable String description) throws Exception {
        final @NotNull Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Project.toProjectsDTO(bootstrap.getProjectService().findByPart(description, session.getUserId()));
    }
}
