package ru.kombarov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.ITaskEndpoint;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.datatransfer.SessionDTO;
import ru.kombarov.tm.datatransfer.TaskDTO;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.kombarov.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    @WebMethod
    public void persistTask(final @Nullable SessionDTO sessionDTO, final @Nullable TaskDTO taskDTO) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getTaskService().persist(TaskDTO.toTask(bootstrap, taskDTO));
    }

    @Override
    @WebMethod
    public void mergeTask(final @Nullable SessionDTO sessionDTO, final @Nullable TaskDTO taskDTO) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getTaskService().merge(TaskDTO.toTask(bootstrap, taskDTO));
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasks(final @Nullable SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().findAll());
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO findOneTask(final @Nullable SessionDTO sessionDTO, final @Nullable String id) throws Exception {
        validateSession(sessionDTO);
        return Task.toTaskDTO(bootstrap.getTaskService().findOne(id));
    }

    @Override
    @WebMethod
    public void removeTask(final @Nullable SessionDTO sessionDTO, final @Nullable String id) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getTaskService().remove(id);
    }

    @Override
    @WebMethod
    public void removeAllTasks(final @Nullable SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getTaskService().removeAll();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskDTO findTasksByName(final @Nullable SessionDTO sessionDTO, final @Nullable String name) throws Exception {
        final @NotNull Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Task.toTaskDTO(bootstrap.getTaskService().findByName(session.getUserId(), name));
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> getTaskListByProjectId(final @Nullable SessionDTO sessionDTO, final @Nullable String projectId) throws Exception {
        final @NotNull Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().getTasksByProjectId(session.getUserId(), projectId));
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findAllTasksByUserId(final @Nullable SessionDTO sessionDTO) throws Exception {
        final @NotNull Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().findAll(session.getUserId()));
    }

    @Nullable
    @Override
    @WebMethod
    public TaskDTO findOneTaskByUserId(final @Nullable SessionDTO sessionDTO, final @Nullable String id) throws Exception {
        final @NotNull Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Task.toTaskDTO(bootstrap.getTaskService().findOne(session.getUserId(), id));
    }

    @Override
    @WebMethod
    public void removeAllTasksByUserId(final @Nullable SessionDTO sessionDTO) throws Exception {
        final @NotNull Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        bootstrap.getTaskService().removeAll(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> sortTasksByDateStart(final @Nullable SessionDTO sessionDTO, final @Nullable String userId) throws Exception {
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().sortByDateStart(userId));
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> sortTasksByDateFinish(final @Nullable SessionDTO sessionDTO, final @Nullable String userId) throws Exception {
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().sortByDateFinish(userId));
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> sortTasksByStatus(final @Nullable SessionDTO sessionDTO, final @Nullable String userId) throws Exception {
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().sortByStatus(userId));
    }

    @NotNull
    @Override
    @WebMethod
    public List<TaskDTO> findTasksByPart(final @Nullable SessionDTO sessionDTO, final @Nullable String description) throws Exception {
        final @NotNull Session session = SessionDTO.toSession(bootstrap, sessionDTO);
        validateSession(sessionDTO);
        return Task.toTasksDTO(bootstrap.getTaskService().findByPart(description, session.getUserId()));
    }
}
