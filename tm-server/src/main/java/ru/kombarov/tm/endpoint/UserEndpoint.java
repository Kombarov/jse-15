package ru.kombarov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.IUserEndpoint;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.datatransfer.SessionDTO;
import ru.kombarov.tm.datatransfer.UserDTO;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.enumerated.Role;
import ru.kombarov.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

import static ru.kombarov.tm.constant.AppConstant.CICLE;
import static ru.kombarov.tm.constant.AppConstant.SALT;

@NoArgsConstructor
@WebService(endpointInterface = "ru.kombarov.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    @WebMethod
    public void persistUser(final @NotNull String login, final @NotNull String password, final @NotNull Role role) throws Exception {
        User user = new User();
        user.setLogin(login);
        user.setPassword(SignatureUtil.sign(password, SALT, CICLE));
        user.setRole(role);
        bootstrap.getUserService().persist(user);
    }

    @Override
    @WebMethod
    public void mergeUser(final @Nullable SessionDTO sessionDTO, final @Nullable UserDTO userDTO) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getUserService().merge(UserDTO.toUser(bootstrap, userDTO));
    }

    @NotNull
    @Override
    @WebMethod
    public List<UserDTO> findAllUsers(final @Nullable SessionDTO sessionDTO) throws Exception {
        return User.toUsersDTO(bootstrap.getUserService().findAll());
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO findOneUser(final @Nullable SessionDTO sessionDTO, final @Nullable String id) throws Exception {
        validateSession(sessionDTO);
        return User.toUserDTO(bootstrap.getUserService().findOne(id));
    }

    @Override
    @WebMethod
    public void removeUser(final @Nullable SessionDTO sessionDTO, final @Nullable String id) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getUserService().remove(id);
    }

    @Override
    @WebMethod
    public void removeAllUsers(final @Nullable SessionDTO sessionDTO) throws Exception {
        validateSession(sessionDTO);
        bootstrap.getUserService().removeAll();
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO getUserByUserName(final @Nullable SessionDTO sessionDTO, final @Nullable String name) throws Exception {
        validateSession(sessionDTO);
        return User.toUserDTO(bootstrap.getUserService().getUserByLogin(name));
    }
}
