package ru.kombarov.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.datatransfer.TaskDTO;
import ru.kombarov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@NoArgsConstructor
@Table(name = "app_task")
public final class Task extends AbstractEntity {

    @Basic
    @Getter
    @Setter
    @Nullable
    private String name;

    @Basic
    @Getter
    @Setter
    @Nullable
    private String projectId;

    @Getter
    @Setter
    @NotNull
    @Basic(optional = false)
    private String userId;

    @Basic
    @Getter
    @Setter
    @Nullable
    private String description;

    @Basic
    @Getter
    @Setter
    @Nullable
    @JsonFormat(pattern = "dd.MM.yyyy")
    private Date dateStart;

    @Basic
    @Getter
    @Setter
    @Nullable
    @JsonFormat(pattern = "dd.MM.yyyy")
    private Date dateFinish;

    @Getter
    @Setter
    @NotNull
    @Basic(optional = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    public Task(final @Nullable String name) {
        this.name = name;
        this.status = Status.PLANNED;
    }

    @NotNull
    public static TaskDTO toTaskDTO(final @NotNull Task task) {
        final @NotNull TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setProjectId(task.getProjectId());
        taskDTO.setUserId(task.getUserId());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDateStart(task.getDateStart());
        taskDTO.setDateFinish(task.getDateFinish());
        taskDTO.setStatus(task.getStatus());
        return taskDTO;
    }

    @NotNull
    public static List<TaskDTO> toTasksDTO(final @NotNull List<Task> tasks) {
        final @NotNull List<TaskDTO> tasksDTO = new ArrayList<>();
        for (Task task : tasks) tasksDTO.add(toTaskDTO(task));
        return tasksDTO;
    }
}
