package ru.kombarov.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.datatransfer.ProjectDTO;
import ru.kombarov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@NoArgsConstructor
@Table(name = "app_project")
public final class Project extends AbstractEntity {

    @Basic
    @Getter
    @Setter
    @Nullable
    private String name;

    @Getter
    @Setter
    @NotNull
    @Basic(optional = false)
    private String userId;

    @Basic
    @Getter
    @Setter
    @Nullable
    private String description;

    @Basic
    @Getter
    @Setter
    @Nullable
    @JsonFormat(pattern = "dd.MM.yyyy")
    private Date dateStart;

    @Basic
    @Getter
    @Setter
    @Nullable
    @JsonFormat(pattern="dd.MM.yyyy")
    private Date dateFinish;

    @Getter
    @Setter
    @NotNull
    @Basic(optional = false)
    @Enumerated(EnumType.STRING)
    private Status status;

    public Project(final @Nullable String name) {
        this.name = name;
        this.status = Status.PLANNED;
    }

    @NotNull
    public static ProjectDTO toProjectDTO(final @NotNull Project project) {
        final @NotNull ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setUserId(project.getUserId());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateStart(project.getDateStart());
        projectDTO.setDateFinish(project.getDateFinish());
        projectDTO.setStatus(project.getStatus());
        return projectDTO;
    }

    @NotNull
    public static List<ProjectDTO> toProjectsDTO(final @NotNull List<Project> projects) {
        List<ProjectDTO> projectsDTO = new ArrayList<>();
        for(Project project : projects) projectsDTO.add(toProjectDTO(project));
        return projectsDTO;
    }
}
