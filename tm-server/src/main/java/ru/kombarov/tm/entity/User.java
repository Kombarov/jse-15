package ru.kombarov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.datatransfer.UserDTO;
import ru.kombarov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "app_user")
public final class User extends AbstractEntity {

    @NotNull
    @Basic(optional = false)
    private String login;

    @NotNull
    @Basic(optional = false)
    private String password;

    @NotNull
    @Basic(optional = false)
    @Enumerated(EnumType.STRING)
    private Role role;

    public User(final @NotNull Role role) {
        this.role = role;
    }

    @NotNull
    public static UserDTO toUserDTO(final @NotNull User user) {
        final @NotNull UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPassword(user.getPassword());
        userDTO.setRole(user.getRole());
        return userDTO;
    }

    @NotNull
    public static List<UserDTO> toUsersDTO(final @NotNull List<User> users) {
        final @NotNull List<UserDTO> usersDTO = new ArrayList<>();
        for(User user : users) usersDTO.add(toUserDTO(user));
        return usersDTO;
    }
}
