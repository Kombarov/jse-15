package ru.kombarov.tm.api.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.User;

import java.util.List;

public interface IUserRepository {

    void persist(final @NotNull User user);

    void merge(final @NotNull User user);

    @NotNull
    List<User> findAll();

    @Nullable
    User findOne(final @NotNull String id);

    void remove(final @NotNull String id);

    void removeAll();

    @Nullable
    User getUserByLogin(final @NotNull String login);
}
