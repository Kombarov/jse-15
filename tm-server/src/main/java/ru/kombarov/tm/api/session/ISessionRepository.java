package ru.kombarov.tm.api.session;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Session;

import java.util.List;

public interface ISessionRepository {

    @NotNull
    List<Session> findAll();

    @Nullable
    Session findOne(final @NotNull String id);

    void persist(final @NotNull Session session);

    void merge(final @NotNull Session session);

    void remove(final @NotNull String id);

    void removeAll();

    @Nullable
    Session findOneByUserId(final @NotNull String userId, final @NotNull String id);

    void removeByUserId(final @NotNull String userId, final @NotNull String id);
}
