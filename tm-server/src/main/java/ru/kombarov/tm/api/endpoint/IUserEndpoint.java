package ru.kombarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.datatransfer.SessionDTO;
import ru.kombarov.tm.datatransfer.UserDTO;
import ru.kombarov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod
    void persistUser(final @NotNull String login, final @NotNull String password, final @NotNull Role role) throws Exception;

    @WebMethod
    void mergeUser(final @Nullable SessionDTO sessionDTO, final @Nullable UserDTO userDTO) throws Exception;

    @NotNull
    @WebMethod
    List<UserDTO> findAllUsers(final @Nullable SessionDTO sessionDTO) throws Exception;

    @Nullable
    @WebMethod
    UserDTO findOneUser(final @Nullable SessionDTO sessionDTO, final @Nullable String id) throws Exception;

    @WebMethod
    void removeUser(final @Nullable SessionDTO sessionDTO, final @Nullable String id) throws Exception;

    @WebMethod
    void removeAllUsers(final @Nullable SessionDTO sessionDTO) throws Exception;

    @Nullable
    @WebMethod
    UserDTO getUserByUserName(final @Nullable SessionDTO sessionDTO, final @Nullable String name) throws Exception;
}
