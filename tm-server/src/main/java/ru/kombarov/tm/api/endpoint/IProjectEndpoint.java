package ru.kombarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.datatransfer.ProjectDTO;
import ru.kombarov.tm.datatransfer.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    void persistProject(final @Nullable SessionDTO sessionDTO, final @Nullable ProjectDTO projectDTO) throws Exception;

    @WebMethod
    void mergeProject(final @Nullable SessionDTO sessionDTO, final @Nullable ProjectDTO projectDTO) throws Exception;

    @NotNull
    @WebMethod
    List<ProjectDTO> findAllProjects(final @Nullable SessionDTO sessionDTO) throws Exception;

    @Nullable
    @WebMethod
    ProjectDTO findOneProject(final @Nullable SessionDTO sessionDTO, final @Nullable String id) throws Exception;

    @WebMethod
    void removeProject(final @Nullable SessionDTO sessionDTO, final @NotNull String id) throws Exception;

    @WebMethod
    void removeAllProjects(final @Nullable SessionDTO sessionDTO) throws Exception;

    @NotNull
    @WebMethod
    ProjectDTO findProjectByName(final @Nullable SessionDTO sessionDTO, final @Nullable String name) throws Exception;

    @NotNull
    @WebMethod
    List<ProjectDTO> findAllProjectsByUserId(final @Nullable SessionDTO sessionDTO) throws Exception;

    @Nullable
    @WebMethod
    ProjectDTO findOneProjectByUserId(final @Nullable SessionDTO sessionDTO, final @Nullable String id) throws Exception;

    @WebMethod
    void removeAllProjectsByUserId(final @Nullable SessionDTO sessionDTO) throws Exception;

    @NotNull
    @WebMethod
    List<ProjectDTO> sortProjectsByDateStart(final @Nullable SessionDTO sessionDTO, final @Nullable String userId) throws Exception;

    @NotNull
    @WebMethod
    List<ProjectDTO> sortProjectsByDateFinish(final @Nullable SessionDTO sessionDTO, final @Nullable String userId) throws Exception;

    @NotNull
    @WebMethod
    List<ProjectDTO> sortProjectsByStatus(final @Nullable SessionDTO sessionDTO, final @Nullable String userId) throws Exception;

    @NotNull
    @WebMethod
    List<ProjectDTO> findProjectsByPart(final @Nullable SessionDTO sessionDTO, final @Nullable String description) throws Exception;
}
